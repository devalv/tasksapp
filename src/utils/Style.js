'use strict';
var React = require('react-native');
var {StyleSheet,} = React;
import { Platform, Dimensions, PixelRatio } from "react-native";
const deviceHeight = Dimensions.get("window").height;
const headerHeight = Platform === "ios" ? (isIphoneX ? 88 : 64) : 56;
module.exports = StyleSheet.create({
    genericView:{
        backgroundColor:'#fff',
        padding:0,
        margin:0,
    },
    mainImageContainer:{
        width:'100%',  
        padding:10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    mainImage:{
        width:'100%',
        height:180,
        marginTop:20,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    br:{
        height:20,
    },
    paddingText:{
        margin:15,
    },
    invisibleList:{
        borderColor:'transparent',
        borderWidth:0,
        height:0,
        width:0
    },
    leftHeader:{
        alignItems:'flex-start'
    },
    righttHeader:{
        alignItems:'flex-end'
    },
    bodyHeader:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    marginTop:{
        marginTop:10,
    },
    center:{
        textAlign:'center',
    },
    centerSpinner:{
        backgroundColor:'#fff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    calendarView:{
        borderWidth: 1,
        borderColor: '#eee',
        marginVertical:15,
    },
    notDone:{
        color:'#000'
    },
    done:{
        color:'#d9e1e8'
    }
});