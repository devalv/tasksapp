import React,{Component} from 'react';
import { Platform } from 'react-native';
import { Router,Stack, Scene, Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import {Button, Text, Icon } from 'native-base';
import Login from '../components/Login';
import TaskDetail from '../components/TaskDetail';
import TaskList from '../components/TaskList';



export default class RouterComponent extends Component{
render(){
    return (
      <Router>
        <Stack key="root" panHandlers={null}>
          <Scene key="login" component={Login} hideNavBar={true} initial={this.props.loggedIn===true?false:true}/>
          <Scene key="tasklist" component={TaskList} hideNavBar={true} initial={this.props.loggedIn===true?true:false}/>
          <Scene key="taskdetail" component={TaskDetail} hideNavBar={true}/>
        </Stack>
      </Router>
    );
  }
}
