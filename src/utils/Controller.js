import firebase from './Config';
import { Permissions, Notifications } from 'expo';
import {  AsyncStorage } from 'react-native';

export let auth = firebase.auth();
export let db = firebase.database();
export function logInUser(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
}
export function logOut(){
    removeItem('uid')
    return auth.signOut();
}
export async function getCurrentUser() {
    let token = getItem('uid');
    if(token !== null){
        return token
    }
    return null;
}
export function getJobs(){
    let ref=db.ref('jobs/');
    return ref.once('value',snapshot=>{
      return snapshot.toJSON(); 
    });
}
export async function getItem(item) {
    try {
      const value = await AsyncStorage.getItem(item);
      return value;
    } catch (error) {
        console.log('error getting item: ', error);
        return null;
    }
    return null;
}
export async function setItem(name,item) {
    try {
      const value = await AsyncStorage.setItem(name,item);
      return value;
    } catch (error) {
        console.log('error setting item: ', error);
        return null;
    }
    return null;
}
export async function removeItem(item) {
    try {
      const value = await AsyncStorage.removeItem(item);
      return value;
    } catch (error) {
        console.log('error removing item: ', error);
        return null;
    }
    return null;
}
export function getToday(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    } 
    if(mm<10) {
        mm = '0'+mm
    } 
    today = yyyy+'-'+mm+'-'+dd;
    return today;
}
export function timeStampToDate(tstmp){
    var today = new Date(tstmp);
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    } 
    if(mm<10) {
        mm = '0'+mm
    } 
    today = yyyy+'-'+mm+'-'+dd;
    return today;
}
export function updateTask(taskKey,uid,id,location){
    let ref=db.ref('jobs/'+taskKey+'/users/'+uid+'/'+id+'/status');
    ref.set('done');
    ref = db.ref('jobs/'+taskKey+'/users/'+uid+'/'+id+'/timestamp');
    let tstp = + new Date();
    ref.set(tstp);
    ref = db.ref('jobs/'+taskKey+'/users/'+uid+'/'+id+'/location');
    ref.set(location)
}