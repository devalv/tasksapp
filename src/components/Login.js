import React, { Component } from 'react' ;
import { AppRegistry, View, Image, AsyncStorage, KeyboardAvoidingView ,Alert,} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import {logInUser,setItem} from '../utils/Controller';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Spinner } from 'native-base';

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      email: '', 
      password: '', 
      isLoading: false 
    };
  }
  onLoginButtonPress() {
    const { email, password } = this.state;
    this.setState({ isLoading: true });
    logInUser(email, password).then(async data => {
      console.log('data: ', data);
      await setItem('uid', data.user.uid);
      Actions.tasklist();
    }).catch((error) => {
      console.log(error);
      this.setState({isLoading:false});
      Alert.alert('Error','Datos Incorrectos');
    });
  }
  componentWillMount(){
    
  }
  render() {
    var style= require('../utils/Style');
    return (
      <Container>
      <View style={style.whiteSpace}></View>
        <Content>
        <KeyboardAvoidingView behavior="padding" enabled>
        <View style={style.mainImageContainer}>
            <Image
                resizeMode="contain"
                style={style.mainImage}
                source={require('../../assets/images/done.png')}
            />
        </View>
        <Form>
          <Item underline style={style.paddingText}>
            <Input autoCorrect={ false }   autoCapitalize = 'none' placeholder='Correo electrónico' onChangeText={ (email) => this.setState({ email }) }/>
          </Item>
          <Item underline style={style.paddingText}>
            <Input placeholder='Contraseña' onChangeText={ (password) => this.setState({ password }) } secureTextEntry />
          </Item>
            {  
              this.state.isLoading ?
              <Spinner size='small' color='black' />
              :
              <View style={style.marginTop}>
                <Button block style={style.paddingText} onPress={ this.onLoginButtonPress.bind(this) }>
                  <Text>Iniciar Sesión</Text>
                </Button>
              </View>
            }
        </Form>
        </KeyboardAvoidingView>
        </Content>
      </Container>
      
    );
  }
}