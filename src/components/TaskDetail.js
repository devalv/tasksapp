import React from 'react';
import { StyleSheet,View,Alert,FlatList } from 'react-native';
import { Container, Header, Title, Content,Spinner, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import {List,ListItem} from 'react-native-elements';
import {getJobs,logOut, getToday, timeStampToDate,getCurrentUser,updateTask} from '../utils/Controller';
import {Actions} from 'react-native-router-flux';
import { Constants, Location, Permissions, } from 'expo';

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state={
      tasks:null,
      task:null,
      uid:null
    };
  }
  getUid(){
    let self = this;
    getCurrentUser().then(uid=>{
      if(uid!==null){
        self.setState({uid})
      }
    }).catch(error=>{
      console.log('error: ', error);

    });
  }
  startOver(){
    if(this.props.tasks!==null && this.props.task!==null){
      this.getUid();
      this.setState({tasks:this.props.tasks,task:this.props.task});
      this._getLocationAsync();
    }
  }
  componentWillReceiveProps(nextProps) { 
    console.log('nextProps: ', nextProps);
    if(nextProps.tasks!==null && nextProps.task!==null){
      this.getUid();
      this.setState({tasks:nextProps.tasks,task:nextProps.task});
      this._getLocationAsync();
    }
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    let currentLocation = [location.coords.latitude, location.coords.longitude];
    this.setState({ location:currentLocation });
  };
  confirm(id){
    let taskKey= this.state.tasks.code;
    let uid = this.state.uid;
    let location = this.state.location.toString();
    updateTask(taskKey,uid,id,location);
    let task = this.state.task;
    Object.entries(task).map(element=>{
      if(element[0]===id){
        element[1].status='done';
      }
    });
    this.setState({task});
  }
  confirmTask(id){
    Alert.alert(
      'Cumplir',
      '¿Está seguro que desea cumplir esta tarea?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Ok', onPress: () => this.confirm(id)},
      ]
    );
  }
  renderItems = (element)=>{
    var style= require('../utils/Style');
    let item = element.item[1];
      return(
        <ListItem 
          title={item.title}
          subtitle={item.description}
          onPress={item.status!=='done'?()=>this.confirmTask(element.item[0]):()=>console.log('already done')}
          titleStyle={item.status!=='done'?style.notDone:style.done}
          subtitleStyle={item.status!=='done'?style.notDone:style.done}
          rightIcon={item.status!=='done'?<Icon name='ios-alert-outline' color='#57ad68' />:<Icon name='ios-checkmark' color='#57ad68' />}
        />
      )
  }
  componentWillMount(){
    this.startOver();
  }
  render() {
    var style= require('../utils/Style');
      return (
        <Container>
            <Header>
              <Left>
                <Button transparent onPress={()=>Actions.tasklist()}>
                  <Icon name='ios-arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title style={style.center}>{this.state.tasks!==null?this.state.tasks.title:'Detalle'}</Title>
              </Body>
              <Right>
              </Right>
            </Header>
            <Content style={style.genericView}>
              {
                this.state.task!==null ?
                  <List containerStyle ={{borderTopWidth:0,borderBottomWidth:0}}>
                    <FlatList
                        data={Object.entries(this.state.task)}
                        renderItem={this.renderItems}
                        keyExtractor={(item,index) => index.toString()}
                    />
                  </List>
                  :
                  <View style={style.centerSpinner}>
                      <Spinner color='#3F51B5' />
                  </View>
              }
            </Content>
          </Container>
      );
    }
  }