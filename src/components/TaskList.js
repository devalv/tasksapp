import React from 'react';
import { StyleSheet,View,Alert,FlatList } from 'react-native';
import { Container, Header, Title, Content,Spinner, Footer, FooterTab, Button, Left, Right, Body, Icon, Text ,ListItem} from 'native-base';
import {getJobs,logOut, getToday, timeStampToDate,getCurrentUser} from '../utils/Controller';
import {Actions} from 'react-native-router-flux';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state={
      jobs:null,
      date:null,
      uid:null,
      markedDates:{},
      isLoading:true,
    }
  }
  gettingJobs(){
    getJobs().then(value=>{
      let jobs = value.toJSON();
      console.log('jobs: ', jobs);
      this.setState({jobs,isLoading:false})
    })
    .catch(error=>{
      console.log('error: ', error);
      Alert.alert('Error','Error de conexión');
      this.setState({isLoading:false})
    })
  }
  setTodayDate(){
    let date = getToday();
    let markedDates ={
      [date]:{ selected: true }
    };
    this.setState({date,markedDates})
  }
  getUid(){
    let self = this;
    getCurrentUser().then(uid=>{
      if(uid!==null){
        self.setState({uid})
      }
    }).catch(error=>{
      console.log('error: ', error);

    });
  }
  startOver(){
    this.getUid();
    this.gettingJobs();
    this.setTodayDate();
  }
  logout(){
    logOut().then(value=>{
      console.log('value: ', value);
      Actions.login();
    }).catch(error=>{
      console.log('error: ', error);
    })
  }
  confirmLogout = () =>{
    Alert.alert(
      'Cerrar Sesión',
      '¿Está seguro que desea cerrar sesión?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Salir', onPress: () => this.logout()},
      ]
    );
  }
  updateDate = (day) =>{
    const markedDates = Object.assign({}, this.state.markedDates)
    const dateString = day.dateString;
    if (markedDates.hasOwnProperty(dateString)) {
      console.log('/*delete markedDates[dateString]*/');
    } else {
      markedDates[dateString] = { selected: true }
    }
    Object.entries(markedDates).map(element=>{
      if(element[0]!==dateString){
        delete markedDates[element[0]]
      }
    })
    this.setState({ markedDates })
  }
  changeMonth = (month) =>{
    console.log('month: ', month);
    this.setState({date:month.dateString})
  }
  renderItems = (element)=>{
    var style= require('../utils/Style');
    let item = element.item;
    let today = timeStampToDate(item.tsc);
    let markedDates = Object.assign({}, this.state.markedDates);
    
    let date = Object.keys(markedDates);
    let uid = this.state.uid;
    
    if(today === date[0]){
      return Object.entries(item.users).map(element=>{
        if(element[0]===uid){
          return(
            <ListItem key={element[0]} onPress={()=>Actions.taskdetail({tasks:item,task:element[1]})}>
              <Body>
                <Text>{item.title} </Text>
              </Body>
              <Right />
            </ListItem>
          )
        }
      })
    }
    else{
      return <ListItem style={style.invisibleList}></ListItem>
    }
  }
  componentWillMount(){
    this.startOver();
  }
  render() {
    var style= require('../utils/Style');
      return (
        <Container>
          <Header>
            <Body>
              <Title style={style.center}>Tareas</Title>
            </Body>
            <Right>
              <Button transparent onPress={this.confirmLogout}>
                <Icon name='ios-log-out' />
              </Button>
            </Right>
          </Header>
          <Content style={style.genericView}>
          {
            this.state.date!==null&&
            <Calendar    
              current={this.state.date}
              minDate={'2000-01-01'}
              maxDate={'2020-05-30'}
              onDayPress={(day) => this.updateDate(day)}
              onDayLongPress={(day) => this.updateDate(day)}
              monthFormat={'MMMM yyyy'}              
              hideArrows={false}
              hideExtraDays={true}
              markedDates={this.state.markedDates}
              disableMonthChange={true}
              firstDay={1}
              onMonthChange={(month) => {this.changeMonth(month)}}
              onPressArrowLeft={substractMonth => substractMonth()}
              onPressArrowRight={addMonth => addMonth()}
              style={style.calendarView}
              theme={{
                backgroundColor: '#ffffff',
                calendarBackground: '#ffffff',
                textSectionTitleColor: '#3f51b5',
                selectedDayBackgroundColor: '#3f51b5',
                selectedDayTextColor: '#fff',
                todayTextColor: '#3f51b5',
                dayTextColor: '#434343',
                textDisabledColor: '#d9e1e8',
                dotColor: '#00adf5',
                selectedDotColor: '#3f51b5',
                arrowColor: '#3f51b5',
                monthTextColor: '#3f51b5',
                textMonthFontWeight: 'bold',
                textDayFontSize: 16,
                textMonthFontSize: 16,
                textDayHeaderFontSize: 16
              }}
            />
          }
          <View>
          {
            this.state.isLoading===true?
              <View style={style.centerSpinner}>
                  <Spinner color='#3F51B5' />
              </View>
            :
            <View>
            {
              this.state.jobs!==null &&
                <FlatList
                    data={Object.values(this.state.jobs)}
                    renderItem={this.renderItems}
                    keyExtractor={(item,index) => index.toString()}
                />
            }
            </View>
          }
          </View> 
          </Content>
        </Container>
      );
    }
  }