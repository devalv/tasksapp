import React from 'react';
import {AsyncStorage} from 'react-native';
import getTheme from './native-base-theme/components';  
import { StyleSheet, Text, View } from 'react-native';
import { StyleProvider } from 'native-base';
import Expo from 'expo';
import material from './native-base-theme/variables/material';
import Router from './src/utils/Router';
import {getItem} from './src/utils/Controller';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      isLoading: true, 
      loggedIn:false
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      'Ionicons': require('@expo/vector-icons/fonts/Ionicons.ttf'),
    });
    
    let uid = await getItem('uid');
    if(uid){
      this.setState({loggedIn:true})
    }
    this.setState({isLoading:false})
  }
  render() {
    return (
      <StyleProvider  style={getTheme(material)}>   
      {
        this.state.isLoading?
          <Expo.AppLoading />
        :
          <Router loggedIn={ this.state.loggedIn } />
      }
      </StyleProvider> 
    );
  }
}
